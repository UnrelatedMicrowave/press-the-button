{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}

import Control.Monad
import Data.Array.IO
import Game

handleInput :: Game -> IO ()
handleInput g@(Game tm p@(Player ppos acs)) = do
  drawGame g
  c <- getLine
  case c of
    "up" -> putStrLn "up" >> handleInput (Game tm (movePlayer p UpDir))
    "down" -> putStrLn "down" >> handleInput (Game tm (movePlayer p DownDir))
    "left" -> putStrLn "left" >> handleInput (Game tm (movePlayer p LeftDir))
    "right" -> putStrLn "right" >> handleInput (Game tm (movePlayer p RightDir))
    "quit" -> putStrLn "\nquit"
    "press" -> pressf
    "yell" -> yellf >> handleInput (Game tm (Player ppos (acs ++ [Yell])))
    "lick" -> lickf
    _ -> putStrLn "invalid command" >> handleInput g

  where pressf =
          do
            t <- tm <#> ppos
            if t == Button then
              do
                print "pressed button, you won"
              else
              print "no button there"
              >> handleInput (Game tm (Player ppos (acs ++ [Press])))

        yellf =
          do
            t <- tm <#> ppos
            if t == Button then
              do
                print "you yelled at the button"
              else
              print "you yelled at nothing"

        lickf =
          do
            t <- tm <#> ppos
            if t == Button then
              do
                print "you licked the button, you've died of ligma"
              else
              print "you licked the floor, you've died of ligma"

  
main :: IO()
main =
  do
    tm <- newTileMap 10 10
    g <- return $ Game tm (Player (2, 2) [])

    writeTileMap tm (5, 5) Button

    handleInput g

