{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}

module Game where

import Control.Monad (when)
import Data.Array.IO
import Data.Array.MArray


{- TILES -}
data Tile = Floor | Button
  deriving(Show, Eq)

tileToChar :: Tile -> Char
tileToChar t = case t of
  Floor -> 'f'
  Button -> 'b'

newtype TileMap = TileMap {
  tileMapArray :: IOArray (Int, Int) Tile
  }


newTileMap :: Int -> Int -> IO TileMap
newTileMap mx my =
  newArray ((1, 1), (mx, my)) Floor >>= (\x -> return $ TileMap x)

readTileMap :: TileMap -> (Int, Int) -> IO Tile
readTileMap (TileMap {tileMapArray}) i =
  readArray tileMapArray i

writeTileMap :: TileMap -> (Int, Int) -> Tile -> IO ()
writeTileMap (TileMap{tileMapArray}) i t =
  writeArray tileMapArray i t

(<#>) = readTileMap


generatePrintArray :: TileMap -> IO (IOArray (Int, Int) (IO ()))
generatePrintArray (TileMap arr) =
  mapArray (putChar . tileToChar) arr



{- ACTION -}
data Action = Walk |Press |Yell |Lick
  deriving(Show)


{- PLAYER -}
data Direction = UpDir |DownDir |LeftDir |RightDir
  deriving(Show)

data Player = Player {
  playerPosition :: (Int, Int),
  playerActionHistory :: [Action]
  }
  deriving(Show)

movePlayer :: Player -> Direction -> Player
movePlayer (Player (x, y) as0) dir =
  let as1 = as0 ++ [Walk] in
    case dir of
      UpDir -> Player (x, y - 1) as1
      DownDir -> Player (x, y + 1) as1
      LeftDir -> Player (x - 1, y) as1
      RightDir -> Player (x + 1, y) as1


--drawPlayer
 

{- GAME -}

data Game = Game {gameTileMap :: TileMap, gamePlayer :: Player}

newGame :: IO Game
newGame =
  newTileMap 10 10 >>= (\tm -> 
                          return $ Game tm (Player (1, 1) []))

drawGame :: Game -> IO ()
drawGame (Game tm0 (Player (px, py) as)) =
    do
      arr0 <- generatePrintArray tm0
      ((ix, iy), (fx, fy)) <- getBounds arr0

      when (px <= fx && py <= fy && px >= ix && py >= iy) do
        writeArray arr0 (px, py) (putStr "p")

      loopy arr0 (ix, iy) ix fx fy
      putStr "\n"

        where
          loopx :: IOArray (Int, Int) (IO ())
            -> (Int, Int)
            -> Int
            -> IO ()
          loopx arr (x, y) maxx = do
            when (x <= maxx)
              do
                f <- readArray arr (x, y)
                f
                putStr " "
                loopx arr (x + 1, y) maxx
  
          loopy :: IOArray (Int, Int) (IO ())
            -> (Int, Int)
            -> Int
            -> Int
            -> Int
            -> IO ()
          loopy arr (x, y) minx maxx maxy = do
            when (y <= maxy)
              do
                loopx arr (x, y) maxx
                putStr "\n"
                loopy arr (minx, y + 1) minx maxx maxy


